import React from 'react';

const Reporter = (props) => {
    return(
        <div>
            <p>{props.name}: </p>
            <p>{props.children}</p>
        </div>
    );
};

export default Reporter;